-- Twenty first task
--   Write a SQL statement to get the maximum purchase amount of all the orders.
--   | Order Id | Maximum Purchase Amount, $ |
 SELECT TOP 1 OD.OrderID, (OD.UnitPrice*OD.Quantity) AS [Maximum Purchase Amount, $]
 FROM [Order Details] OD
 ORDER BY OD.UnitPrice*OD.Quantity DESC