-- Seventh task
--   Create a query to return all employees and full name of person to whom this employee reports to:
--   | EmployeeId | FullName | ReportsTo |
--   Full Name - title of courtesy with full name.
--   Order data by first column.
--   Reports To - Full name. If the employee does not report to anybody leave "-" in the column.
 SELECT E.EmployeeID, (E.FirstName  ' '  E.LastName) As Fullname, ISNULL((EM.FirstName  ' '  EM.LastName), '-') As ReportsTo
 FROM Employees E
 LEFT JOIN Employees EM ON E.ReportsTo=EM.EmployeeID