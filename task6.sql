-- Sixth task
--  Create a query to return all products with category and supplier company names:
--  ProductName | CategoryName | SupplierCompanyName |
SELECT P.ProductName, C.CategoryName, S.CompanyName AS [SupplierCompanyName]
FROM Products P
RIGHT JOIN Suppliers S
ON P.SupplierID = S.SupplierID
INNER JOIN Categories C
ON P.CategoryID = C.CategoryID