-- Third task
--  Create a query to return all customers from USA without Fax:
-- | CustomerId | CompanyName |
SELECT C.CustomerID, C.CompanyName FROM Customers C WHERE C.Country='USA' AND C.Fax IS NOT NULL