-- Nineteenth task
--   Create a SQL query to display customer details whose total orders amount is more than 10000$:
--   | CustomerId | CompanyName | TotalOrdersAmount, $ |
 SELECT C.CustomerID, ISNULL(Tab.[Total Amount], 0) As TotalOrdersAmount 
 FROM Customers C
 LEFT JOIN (
           SELECT O.CustomerID, (OD.UnitPrice*OD.Quantity) AS [Total Amount]
           FROM Orders O
           INNER JOIN [Order Details] OD ON O.OrderID = OD.OrderID
           WHERE OD.UnitPrice*OD.Quantity > 10000
           ) AS Tab
           ON C.CustomerID = Tab.CustomerID
           WHERE  Tab.[Total Amount] <> 0