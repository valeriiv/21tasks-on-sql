-- Fifteenth task
--   Create a SQL query to return the total number of orders for every month in 1997 year:
--   | January | February | March | April | May | June | July | August | September | November | December |
 SELECT DATENAME([MONTH], OrderDate) AS OrderMonth, COUNT(O.OrderId) AS TotalOrdersNum
 FROM Orders O WHERE OrderDate BETWEEN '1997-01-01' AND '1997-12-31'
 GROUP BY DATENAME([MONTH], OrderDate)
 ORDER BY [OrderMonth]