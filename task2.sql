-- Second task
-- Create a query to return a list ordered by order id descending
-- | Order Id | Order Total Price | Total Order Discount, % |
SELECT OD.OrderID, (OD.UnitPrice * OD.Quantity) AS OrderTotalPrice, OD.Discount AS [TotalOrderDiscount, %]
FROM [Order Details] OD
ORDER BY OD.OrderID DESC
--  alternative
SELECT OD.OrderID, SUM(OD.UnitPrice) AS TotalCount, SUM(OD.Discount)*100 AS TotalDiscount 
FROM [Order Details] OD
GROUP BY OD.OrderId 
ORDER BY OD.OrderID DESC