-- Twenty second task
--   Create a SQL query to display the name of each customer along with their most expensive purchased product:
--   | CompanyName | ProductName | PricePerItem |
 SELECT C.CustomerID, MAX(ISNULL(Tab.[Total Amount], 0)) AS TotalOrdersAmount
 FROM Customers C
 LEFT JOIN (
          SELECT O.CustomerID, (OD.UnitPrice*OD.Quantity) AS [Total Amount]
          FROM Orders O
          INNER JOIN [Order Details] OD ON O.OrderID = OD.OrderID
           ) AS Tab
          ON C.CustomerID = Tab.CustomerID
          WHERE  Tab.[Total Amount]<> 0
          GROUP BY C.CustomerID 