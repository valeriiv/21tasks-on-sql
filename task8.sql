-- Eighth task
--   Create a query to return:
--   | CategoryName | TotalNumberOfProducts |
 SELECT C.CategoryName, COUNT(P.ProductID) AS TotalNumberOfProducts 
 FROM Categories C
 INNER JOIN Products P
  in this case all JOIN clause return the same result
 LEFT JOIN Products P
 RIGHT JOIN Products P
 FULL OUTER JOIN Products P
 ON C.CategoryID = P.CategoryID
 GROUP BY C.CategoryName