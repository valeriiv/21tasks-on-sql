--  Fifth task
--  Return all products where product name starts with 'A', 'B', .... 'F' ordered by name.
--  | ProductId | ProductName | QuantityPerUnit |
SELECT P.ProductID, P.ProductName, P.QuantityPerUnit 
FROM Products P 
WHERE P.ProductName LIKE 'a%' 
OR P.ProductName LIKE 'b%'
OR P.ProductName like 'c%'
OR P.ProductName like 'd%'
OR P.ProductName like 'e%'
OR P.ProductName like 'f%'