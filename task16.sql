-- Sixteenth task
--   Create a SQL query to return all orders where ship postal code is provided:
--   | OrderId | CustomerId | ShipCountry |
 SELECT O.OrderID, O.CustomerID, O.ShipCountry 
 FROM Orders O WHERE O.ShipPostalCode IS NOT NULL