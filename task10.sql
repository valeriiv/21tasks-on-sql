-- Tenth task
--   Write a query to get discontinued Product list:
--   | ProductId | Name |
 SELECT P.ProductID AS ProductId, P.ProductName AS Name 
 FROM Products P 
 WHERE P.Discontinued = 0