-- Forrteenth task
--   Create a SQL query to get Product list of stock is less than the quantity on order:
--   | ProductName | UnitsInOrder| UnitsInStock |
 SELECT P.ProductName, P.UnitsOnOrder AS UnitsInOrder, P.UnitsInStock 
 FROM Products P 
 WHERE P.UnitsOnOrder > P.UnitsInStock
 ORDER BY P.UnitsInStock