-- Twenteenth task
--   Write a SQL query to get Product list of twenty most expensive products:
--   | ProductName | UnitPrice |
--   Order products by price.
 SELECT TOP 12 P.ProductName, P.UnitPrice 
 FROM Products P
 ORDER BY P.UnitPrice