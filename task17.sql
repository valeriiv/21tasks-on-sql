-- Seventeenth task
--   Create SQL query to display the average price of each categories's products:
--   | CategoryName | AvgPrice |
 SELECT C.CategoryName, AVG(P.UnitPrice) AS AvgPrice
 FROM Categories C
 INNER JOIN Products P
 ON C.CategoryID = P.CategoryID
 GROUP BY C.CategoryName
 ORDER BY [AvgPrice]