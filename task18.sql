-- Eighteenth task
--   Create a SQL query to calcualte total orders count by each day in 1998:
--   | Order Date | Total Number of Orders |
 SELECT O.OrderDate, COUNT(O.OrderID) AS [Total Number of Orders] 
 FROM Orders O 
 WHERE O.OrderDate >= '1998' AND O.OrderDate <= '1999'
 GROUP BY O.OrderDate