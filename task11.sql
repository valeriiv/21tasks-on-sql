-- Eleventh task
--   Create a SQL query to get Product list (name, unit price) where products cost between $5 and $15:
--   | ProductName | UnitPrice |
 SELECT P.ProductName, P.UnitPrice 
 FROM Products P 
 WHERE P.UnitPrice 
 BETWEEN 5 AND 15 
 ORDER BY P.UnitPrice