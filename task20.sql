-- Twentieth task
--   Create a SQL query to find the employee that sold products for the largest amount:
--   | EmployeeId | Employee Full Name | Amount, $ |
 SELECT TOP 1 E.EmployeeID, (E.FirstName  ' '  E.LastName) AS [Employee Full Name], ISNULL(Tab.[Total Amount], 0) AS Amount
 FROM Employees E
 LEFT JOIN (
          SELECT O.EmployeeID, (OD.UnitPrice*OD.Quantity) AS [Total Amount]
          FROM Orders O
          INNER JOIN [Order Details] OD ON O.OrderID = OD.OrderID
           ) AS Tab
          ON E.EmployeeID = Tab.EmployeeID
          ORDER BY Amount DESC