-- Thirteenth task
--   Create a SQL query to count current and discontinued products:
--   | TotalOfCurrentProducts | TotalOfDiscontinuedProducts |
 SELECT COUNT(P.Discontinued) AS TotalOfCurrentProducts, COUNT(CASE WHEN (P.Discontinued = 0) THEN 1 ELSE NULL END) AS TotalOfDiscontinuedProducts 
 FROM Products P